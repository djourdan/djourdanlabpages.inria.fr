+++
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
categories = [
    "publications"
]
authors = ""
conference = ""
paper = "/papers/paper.pdf"
image = "/img/img.jpg"
+++


