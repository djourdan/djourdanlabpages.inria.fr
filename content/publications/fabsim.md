+++
title = "Fabsim"
date = "2021-01-01"
categories = [
    "software"
]
link = "https://github.com/DavidJourdan/fabsim"
image = "/img/fabsim.png"
+++

Fabsim is a small simulation library written in C++ using Eigen. It comprises different material models for simulating rods, membranes and shells and is meant to be easy to use.


