+++
authors = "David Jourdan"
title = "Computational design of self-shaping textiles"
date = "2022-06-18"
categories = [
    "publications"
]
conference = "PhD thesis"
image = "/img/thesis.jpeg"
paper = "/papers/thesis.pdf"
bibtex = "/papers/thesis-bib.html"
+++


