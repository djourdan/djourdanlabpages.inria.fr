+++
authors = "David Jourdan, Mélina Skouras, Etienne Vouga, Adrien Bousseau"
title = "Printing-on-Fabric Meta-Material for Self-Shaping Architectural Models"
date = "2020-07-19"
categories = [
    "publications"
]
conference = "Advances in Architectural Geometry 2020"
video = "https://www.youtube.com/watch?v=0ap8hNkBNxI"
link = "http://www-sop.inria.fr/reves/Basilic/2020/JSVB20/"
paper = "/papers/self-shaping-stars.pdf"
image = "/img/aag-teaser.jpg"
bibtex = "/papers/self-shaping-stars-bib.html"
+++


