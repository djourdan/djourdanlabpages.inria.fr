+++
authors = "David Jourdan, Mélina Skouras, Adrien Bousseau"
title = "Optimizing Support Structures for Tensile Architecture"
date = "2018-11-16"
categories = [
    "publications"
]
conference = "Journées Françaises d'Informatique Graphique 2018"
video = ""
link = ""
paper = "/papers/tensile-structures-jfig.pdf"
bibtex = "/papers/tensile-structures-jfig.html"
image = "/img/jfig.png"
+++
