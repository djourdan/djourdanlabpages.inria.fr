+++
authors = "David Jourdan, Mélina Skouras, Etienne Vouga, Adrien Bousseau"
title = "Computational Design of Self-Actuated Surfaces by Printing Plastic Ribbons on Stretched Fabric"
date = "2021-12-09"
categories = [
    "publications"
]
conference = "Eurographics 2022"
video = "https://youtu.be/CSYhhfOgRxg"
link = "http://www-sop.inria.fr/reves/Basilic/2022/JSVB22/"
image = "/img/eg-teaser.png"
paper = "/papers/stripeup-eg.pdf"
supplemental = "https://youtu.be/uj7xokqamuw"
bibtex = "/papers/stripeup-eg.html"
+++


