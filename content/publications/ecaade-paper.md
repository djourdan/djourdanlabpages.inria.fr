+++
authors = "Asterios Agkathidis, David Jourdan, Yang Song, Arathi Kanmani, Ansha Thomas"
title = "Four-dimensional Printing on Textiles: Evaluating digital file-to-fabrication workflows for self-forming composite shell structures"
date = "2023-07-12"
categories = [
    "publications"
]
conference = "Education and research in Computer Aided Architectural Design in Europe (eCAADe) 2023"
image = "/img/ecaade-teaser.png"
bibtex = "/papers/4D-printing-bib.html"
paper = "/papers/4D-printing.pdf"
+++


