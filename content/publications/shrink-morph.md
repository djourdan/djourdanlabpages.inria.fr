+++
authors = "David Jourdan, Pierre-Alexandre Hugron, Camille Schreck, Jonàs Martínez, Sylvain Lefebvre"
title = "Shrink & Morph: 3D-printed self-shaping shells actuated by a shape memory effect"
date = "2023-09-27"
categories = [
    "publications"
]
conference = "SIGGRAPH Asia 2023"
video = "https://youtu.be/YNufMqcDk5I"
image = "/img/shrink-morph-teaser.jpg"
paper = "/papers/shrink-morph.pdf"
bibtex = "/papers/shrink-morph-bib.html"
code = "https://github.com/DavidJourdan/shrink-morph"
+++


