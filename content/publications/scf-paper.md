+++
authors = "David Jourdan, Victor Romero, Etienne Vouga, Adrien Bousseau, Mélina Skouras"
title = "Simulation of printed-on-fabric assemblies"
date = "2022-09-18"
categories = [
    "publications"
]
conference = "Symposium on Computational Fabrication (SCF) 2022"
image = "/img/bilayer-sim-teaser.jpeg"
bibtex = "/papers/bilayer-sim.html"
paper = "/papers/bilayer-sim.pdf"
+++


