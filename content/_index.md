---
title: ""
date: 2020-08-11T12:40:33+02:00
draft: false
---

I'm a researcher focusing in the computational design and fabrication of self-actuated structures, I'm  broadly interested in computer graphics, geometry processing and simulation and especially in exploiting the links between geometry and physics to create computational design algorithms.
I'm currently working as a postdoc in the [Morphing Matter Lab](https://morphingmatter.org/) in UC Berkeley.

You can find my CV [here](misc/CV.pdf)
